const fs = require('fs');
const renderFeedback = require('./render');
const calculateScores = require('./calculateScores');
let json = Buffer.alloc(0);

const scores = {
    "LEVEL_1": {
        "CRUD": {
            "CREATE": 1,
            "READ": 0.5,
            "READ_BY_ID": 0.5,
            "UPDATE": 1,
            "DELETE": 0.5,
            "USER_EXIST_EMAIL": 0.5,
            "USER_EXIST_PHONE": 0.5
        }
    },
    "LEVEL_2": {
        "VALIDATION_CREATE_USER": {
            "WRONG_EMAIL": 1,
            "WRONG_PHONE": 1,
            "ID_IS_PRESENT": 1,
            "FIRST_NAME_ABSENT": 1,
            "LAST_NAME_ABSENT": 1,
            "EMAIL_ABSENT": 1,
            "PHONE_NUMBER_ABSENT": 1,
            "EXTRA_FIELD": 1,
        }
    },
    "LEVEL_3": {
        "VALIDATION_UPDATE_USER": {
            "WRONG_EMAIL": 1,
            "WRONG_PHONE": 1,
            "ID_IS_PRESENT": 1,
            "EXTRA_FIELD": 1,
            "USER_DOESNT_EXIST": 0.5
        }
    },
    "LEVEL_4": {
        "CRUD": {
            "CREATE": 1,
            "READ": 0.5,
            "READ_BY_ID": 0.5,
            "UPDATE": 1,
            "DELETE": 0.5,
            "FIGHTER_EXIST_NAME": 0.5
        }
    },
    "LEVEL_5": {
        "VALIDATION_CREATE_FIGHTER": {
            "NAME_ABSENT": 1,
            "POWER_ABSENT": 1,
            "ID_IS_PRESENT": 1,
            "POWER_IS_STRING": 1,
            "EXTRA_FIELD": 1,
            "POWER_MORE_100": 1,
            "POWER_MORE_0": 1,
            "POWER_MORE_NEGATIVE": 1,
            "DEFENSE_IS_STRING": 1,
            "DEFENSE_MORE_10": 1,
            "DEFENSE_LESS_1": 1,
            "DEFENSE_ABSENT": 1
        }
    },
    "LEVEL_6": {
        "VALIDATION_UPDATE_FIGHTER": {
            "ID_IS_PRESENT": 1,
            "POWER_IS_STRING": 1,
            "EXTRA_FIELD": 1,
            "POWER_MORE_100": 1,
            "POWER_MORE_0": 1,
            "POWER_MORE_NEGATIVE": 1,
            "DEFENSE_IS_STRING": 1,
            "DEFENSE_MORE_10": 1,
            "DEFENSE_LESS_1": 1
        }
    },
}

const levelsCount = Object.keys(scores).length;
let maxCount = 0;
Object.keys(scores).forEach(lvlKey => {
    Object.keys(scores[lvlKey]).forEach(stKey => {
        Object.keys(scores[lvlKey][stKey]).forEach(key => {
            maxCount += scores[lvlKey][stKey][key];
        });
    });
});

let language = "ua";
const TOKEN = process.argv[2];
const LOCALIZATION_VAR = process.argv[3] ? process.argv[3].toLowerCase() : 'ua';


if (LOCALIZATION_VAR === 'ru') {
	language = 'ru';
}

if (LOCALIZATION_VAR === 'en') {
	language = 'en';
}

process.stdin.setEncoding('utf8');
process.stdin.on('data', (data) => {
	json = Buffer.concat([ json, Buffer.from(data) ]);
});
  
process.stdin.on('end', () => {
    const utf = json.toString('utf8');
    let str = utf;

    str = str.replace(/undefined:1/g, '');
    str = str.replace(/undefined/g, '');

    // const first = utf.indexOf('{');
    // const end = utf.lastIndexOf('}');
    // const str = utf.substr(first, end - first + 1);

	const testResult = JSON.parse(str);
    const localization = require('./localization/' + language);
    const calculatedResult = calculateScores(testResult, scores, maxCount / levelsCount);
    const result = renderFeedback(localization, calculatedResult);
    
    fs.writeFileSync(__dirname + '/../../result.txt', result, { flag: 'w+' });

    const mark = parseFloat(Number(calculatedResult.stats.score).toFixed(1));

	fs.writeFileSync(__dirname + '/../../result.json', JSON.stringify({
		"token": TOKEN,
        "mark": mark,
        "trace": result,
        "buildNumber": process.env.BUILD_NUMBER,
		"generatedFeedback": getFeedbackByMark(mark, localization)
    }, null, 4), { flag: 'w+' });
});

const getFeedbackByMark = (mark, localization) => {
    if(mark <= 5) {
        return localization["IMPROVEMENTS_ARE_REQUIRED"];
    }

    // if(mark > 5 && mark <= 7) {
    //     return localization["YOU_CAN_BETTER"];
    // }

    if(mark > 5 && mark <= 7) {
        return localization["WELL_DONE"];
    }

    if(mark > 7) {
        return localization["EXCELLENT"];
    }
}