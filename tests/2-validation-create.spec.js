const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { userToInsert } = require('./testData');

chai.use(chaiHttp);

console.log = () => {};

const { app } = require('../index');

const url = '/api/users'

describe('LEVEL_2', () => {
    const testUser = {...userToInsert};

    const wrongPhone = '80501234567';
    const wrongEmails = ['some', 'some@', '@', 'some@.com', 'some@dfsdf'];

    describe('VALIDATION_CREATE_USER', () => {
        it('WRONG_EMAIL', (done) => {
            try {
                wrongEmails.forEach((testEmail) => {
                    const user = {...userToInsert, email: testEmail}
                    chai
                        .request(app)
                        .post(url)
                        .send(user)
                        .end((err, res) => {
                            try {
                                expect(res).to.have.status(400);
                            } catch (err) {
                                done(err);
                            }
                        });
                });
                done();
            } catch(err) {
                done(err);
            }
        });

        it('WRONG_PHONE', (done) => {
            const user = {...testUser, phoneNumber: wrongPhone}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('ID_IS_PRESENT', (done) => {
            const user = {...testUser, id: 111}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('FIRST_NAME_ABSENT', (done) => {
            const user = {...testUser, firstName: undefined}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('LAST_NAME_ABSENT', (done) => {
            const user = {...testUser, lastName: undefined}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('EMAIL_ABSENT', (done) => {
            const user = {...testUser, email: undefined}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('PHONE_NUMBER_ABSENT', (done) => {
            const user = {...testUser, phoneNumber: undefined}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('EXTRA_FIELD', (done) => {
            const user = {...testUser, randmoField: 'random'}
            chai
                .request(app)
                .post(url)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });
        
    });
});