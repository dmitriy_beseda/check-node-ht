const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { userToInsert } = require('./testData');

chai.use(chaiHttp);

console.log = () => {};

const { app } = require('../index');

const responseSuccess = (err, res) => {
    expect(err).to.be.null;
    expect(res).to.have.status(200);
    return JSON.parse(res.text);
};

const responseSuccessDelete = (err, res) => {
    expect(err).to.be.null;
    if(res.status === 204) {
        expect(res).to.have.status(204);
    } else {
        expect(res).to.have.status(200);
        return JSON.parse(res.text);
    }
    return {};
};

const url = '/api/users';

describe('LEVEL_1', () => {
    const testUser = {...userToInsert};

    describe('CRUD', () => {
        it('CREATE', (done) => {
            chai
                .request(app)
                .post(url)
                .send(testUser)
                .end((err, res) => {
                    data = responseSuccess(err, res);
                    try {
                        Object.keys(testUser).forEach(key => {
                            expect(data[key]).to.be.equal(testUser[key]);
                        });
                        testUser.id = data.id;
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('READ', (done) => {
            chai
                .request(app)
                .get(url)
                .end((err, res) => {
                    try {
                        const users = responseSuccess(err, res);
                        expect(users.length).equal(1);

                        const insertedUser = users[0];

                        Object.keys(testUser).forEach(key => {
                            expect(insertedUser[key]).to.be.equal(testUser[key]);
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('READ_BY_ID', (done) => {
            chai
                .request(app)
                .get(`${url}/${testUser.id}`)
                .end((err, res) => {
                    try {
                        const response = responseSuccess(err, res);
                        Object.keys(testUser).forEach(key => {
                            expect(response[key]).to.be.equal(testUser[key]);
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('UPDATE', (done) => {
            const keyToUpdate = 'lastName';
            const dataToUpdate = {};
            dataToUpdate[keyToUpdate] = 'newlastname';

            chai
                .request(app)
                .put(`${url}/${testUser.id}`)
                .send(dataToUpdate)
                .end((err, res) => {
                    try {
                        const response = responseSuccess(err, res);
                        Object.keys(testUser).forEach(key => {
                            if (key !== keyToUpdate) {
                                expect(response[key]).to.be.equal(testUser[key]);
                            } else {
                                expect(response[key]).equal(dataToUpdate[key]);
                            }
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('USER_EXIST_EMAIL', (done) => {
            const existinUser = {...testUser, email: 'ELONmask@gmail.com', phoneNumber: '+380501112233'};
            chai
                .request(app)
                .post(url)
                .send(existinUser)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('USER_EXIST_PHONE', (done) => {
            const existinUser = {...testUser, email: 'ELONmask1@gmail.com'};
            chai
                .request(app)
                .post(url)
                .send(existinUser)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DELETE', (done) => {
            chai
                .request(app)
                .delete(`${url}/${testUser.id}`)
                .end((err, res) => {
                    try {
                        responseSuccessDelete(err, res);
                    } catch (err) {
                        done(err);
                    }
                });

            chai
                .request(app)
                .get(`${url}/${testUser.id}`)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(404);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });
    });
});
