const isEmpty = require('lodash.isempty');

const getTotal = scores => {
	return Object.keys(scores).reduce((result, key) => result + scores[key], 0)
};

const calculate = (filteredTests, scores) => {
	return filteredTests.reduce((result, test) => {
		const testScore = isEmpty(test.err) ? scores[test.title] : 0;

		return result + testScore;
	}, 0);
};

const filterTests = (tests, prefix) => {
	return tests.filter(test => test.fullTitle.startsWith(prefix));
};

const getMessage = (error) => {
	return error.message + (error.showDiff ? '\n\tExpected: ' + error.expected + '\n\tActual: ' + error.actual : '');
};

const getErrorTests = (tests) => {
	return tests.filter(test => !isEmpty(test.err)).reduce((result, test) => {
		return Object.assign({}, result, {
			[test.title]: {
				message: getMessage(test.err)
			}
		});
	}, {});
};

const calculateScoreByResult = (tests) => {
	return Object.keys(tests).reduce((result, test) => {
		return result + tests[test].stats.score;
	}, 0);
};

const getTotalByResult = tests => Object.keys(tests).reduce((result, test) => result + tests[test].stats.total, 0);

const calculateScores = (testResult, scores, maxScore) => {
	const tests = Object.keys(scores).reduce((result, level) => {
		const tests = Object.keys(scores[level]).reduce((result, testSuit) => {
			const tests = filterTests(testResult.tests, `${level} ${testSuit}`);
			const total = getTotal(scores[level][testSuit]);
			const score = calculate(tests, scores[level][testSuit]);

			return Object.assign({}, result, {
				[testSuit]: {
					stats: {
						total,
						score,
					},
					tests: getErrorTests(tests),
				}
			});
		}, {});
		const score = calculateScoreByResult(tests);
		const total = getTotalByResult(tests);

		return Object.assign({}, result, {
			[level]: {
				stats: {
					score,
					total,
				},
				tests,
			}
		});
	}, {});
	const score = calculateScoreByResult(tests) / getTotalByResult(tests);

	return {
		stats: {
			score: score * maxScore,
			total: maxScore
		},
		tests
	};
};

module.exports = calculateScores;