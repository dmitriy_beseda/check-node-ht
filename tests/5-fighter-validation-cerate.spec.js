const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { fighterToInsert } = require('./testData');

console.log = () => {};

chai.use(chaiHttp);

const { app } = require('../index');

const url = '/api/fighters';

describe('LEVEL_5', () => {
    const testFighter = {...fighterToInsert};

    describe('VALIDATION_CREATE_FIGHTER', () => {

        it('ID_IS_PRESENT', (done) => {
            const fighter = {...testFighter, id: 111}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('NAME_ABSENT', (done) => {
            const fighter = {...testFighter, name: undefined}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_ABSENT', (done) => {
            const fighter = {...testFighter, power: undefined}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_100', (done) => {
            const fighter = {...testFighter, power: 101}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_IS_STRING', (done) => {
            const fighter = {...testFighter, power: 'fifty five'}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_0', (done) => {
            const fighter = {...testFighter, power: 0}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_NEGATIVE', (done) => {
            const fighter = {...testFighter, power: -1}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_ABSENT', (done) => {
            const fighter = {...testFighter, defense: undefined}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_IS_STRING', (done) => {
            const fighter = {...testFighter, defense: 'fifty five'}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_MORE_10', (done) => {
            const fighter = {...testFighter, defense: 101}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_LESS_1', (done) => {
            const fighter = {...testFighter, defense: 0}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('EXTRA_FIELD', (done) => {
            const fighter = {...testFighter, randmoField: 'random'}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });
        
    });
});