const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { fighterToInsert } = require('./testData');

console.log = () => {};

chai.use(chaiHttp);

const { app } = require('../index');

const responseSuccess = (err, res) => {
    expect(err).to.be.null;
    expect(res).to.have.status(200);
    return res.status === 200 ? JSON.parse(res.text) : null;
};

const responseSuccessDelete = (err, res) => {
    expect(err).to.be.null;
    if(res.status === 204) {
        expect(res).to.have.status(204);
    } else {
        expect(res).to.have.status(200);
        return JSON.parse(res.text);
    }
    return {};
};

const url = '/api/fighters';

describe('LEVEL_6', () => {
    const testFighter = {...fighterToInsert};

    describe('VALIDATION_UPDATE_FIGHTER', () => {
        before((done) => {
            chai
                .request(app)
                .post(url)
                .send(testFighter)
                .end((err, res) => {
                    try {
                        data = responseSuccess(err, res);
                        Object.keys(testFighter).forEach(key => {
                            expect(data[key]).to.be.equal(testFighter[key]);
                        });
                        testFighter.id = data.id;
                        done();
                    } catch (err) {
                        done();
                    }
                });
        });

        it('POWER_IS_STRING', (done) => {
            const fighter = {...testFighter, power: 'fifty five'}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_100', (done) => {
            const fighter = {...testFighter, power: 101}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_0', (done) => {
            const fighter = {...testFighter, power: 0}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('POWER_MORE_NEGATIVE', (done) => {
            const fighter = {...testFighter, power: -1}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_IS_STRING', (done) => {
            const fighter = {...testFighter, defense: 'fifty five'}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_MORE_10', (done) => {
            const fighter = {...testFighter, defense: 101}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DEFENSE_LESS_1', (done) => {
            const fighter = {...testFighter, defense: 0}
            chai
                .request(app)
                .post(url)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('ID_IS_PRESENT', (done) => {
            const fighter = {...testFighter, id: 111}
            chai
                .request(app)
                .put(`${url}/${testFighter.id}`)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('EXTRA_FIELD', (done) => {
            const fighter = {...testFighter, randmoField: 'random'}
            chai
                .request(app)
                .put(`${url}/${testFighter.id}`)
                .send(fighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        after((done) => {
            chai
                .request(app)
                .delete(`${url}/${testFighter.id}`)
                .send(testFighter)
                .end((err, res) => {
                    data = responseSuccessDelete(err, res);
                    done();
                });
        });
        
    });
});