#!/bin/bash

if [ -d ./checkrepo ]; then
	rm -rf ./checkrepo
fi

if [ -f ./result.json ]; then
	rm ./result.json
fi

if [ -f ./result.txt ]; then
	rm ./result.txt
fi

read -p "Paste the repo: " GITHUB_URL

REPO_EXISTS=$(. check-repo.sh $GITHUB_URL)

if [ $REPO_EXISTS == true ]
then
	echo 'Repo exists'
else
	$(. send-error.sh $TOKEN)
	return
fi

echo "CONTINUE"

CHECKFOLDER="checkrepo"

# #Clone repo
mkdir $CHECKFOLDER
cd ./$CHECKFOLDER
git clone $GITHUB_URL .

# #COPY TESTS
cp -r ../tests/ ./tests
cp -r ../feedback ./feedback

# #INSTALL DEPENDENCIES
npm i
npm i chai chai-http mocha lodash.isempty

#RUN TESTS

# ./node_modules/.bin/mocha ./tests/**/*.spec.js --exit -R JSON | node ./feedback/index.js $TOKEN $LANG
./node_modules/.bin/mocha ./tests/**/*.spec.js --exit -R JSON | node ./feedback/index.js 123 ru

cd ..
