const greetings = (localization) => {
	return localization.GREETINGS;
};

const getMark = (stats) => `(${+stats.score.toFixed(2)} / ${+stats.total.toFixed(2)})`;

const icon = stats => stats && stats.score === stats.total ? '✔️' : '⚠️';

const perfectMark = stats => stats.score === stats.total ? '🎉' : '';

const renderLevelResult = (localization, testResult, depth = 1) => {
	const padding = '\n' + '\t'.repeat(depth);

	if (testResult.message) {
		return padding + testResult.message.split('\n').join(padding);
	}

	if (testResult.stats.score === testResult.stats.total) {
		return '';
	}

	return padding + Object.keys(testResult.tests).map(key => {
		return `${icon(testResult.tests[key].stats)} ${localization[key]} ${renderLevelResult(
			localization,
			testResult.tests[key],
			depth + 1
		)}`;
	}).join(padding);
};

const renderBody = (localization, testResult) => {
	const tests = testResult.tests || {};

	return localization.GO_THROUGH_THE_LIST + '\n\n' + Object.keys(tests).map((level, i) => {
		return `${i + 1}) ${localization[level]} ${getMark(tests[level].stats)} ${perfectMark(tests[level].stats)}` + renderLevelResult(
			localization,
			tests[level],
		);
	}).join('\n');
};

const getMatrix = (tests) => {
	return [
		+(((tests['LEVEL_1'] || {}).stats || {}).score || 0).toFixed(2),
		+(((tests['LEVEL_2'] || {}).stats || {}).score || 0).toFixed(2),
		+(((tests['LEVEL_3'] || {}).stats || {}).score || 0).toFixed(2),
		+(((tests['LEVEL_4'] || {}).stats || {}).score || 0).toFixed(2),
		'x',
		'x'
	].join('-');
};

// const finalWords = (localization, testResult) => {
// 	let result = '';
// 	if (testResult.stats.score === testResult.stats.total) {
// 		result = localization.RESULT_PERFECT;
// 	} else if (testResult.stats.score > (testResult.stats.total * 0.6)) {
// 		result = localization.RESULT_GOOD;
// 	} else if (testResult.stats.score > 0) {
// 		result = localization.RESULT_BAD;
// 	} else {
// 		result = localization.RESULT_WORST;
// 	}

// 	return result
// 		.replace(/\$\{matrix\}/ig, getMatrix(testResult.tests))
// 		.replace(/\$\{total\}/ig, +testResult.stats.total.toFixed(2))
// 		.replace(/\$\{score\}/ig, +testResult.stats.score.toFixed(2));
// };

module.exports = (localization, testResult) => {
	return [
		greetings(localization),
		renderBody(localization, testResult),
		// finalWords(localization, testResult)
	].join('\n');
};