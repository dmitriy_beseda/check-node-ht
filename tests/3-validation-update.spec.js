const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { userToInsert } = require('./testData');

chai.use(chaiHttp);

console.log = () => {};

const { app } = require('../index');

const responseSuccess = (err, res) => {
    expect(err).to.be.null;
    expect(res).to.have.status(200);
    return res.status === 200 ? JSON.parse(res.text) : null;
};

const responseSuccessDelete = (err, res) => {
    expect(err).to.be.null;
    if(res.status === 204) {
        expect(res).to.have.status(204);
    } else {
        expect(res).to.have.status(200);
        return JSON.parse(res.text);
    }
    return {};
};

const url = '/api/users';

describe('LEVEL_3', () => {
    const testUser = {...userToInsert};

    const wrongPhone = '80501234567';
    const wrongEmails = ['some', 'some@', '@', 'some@.com', 'some@dfsdf'];
    const randomEmails = ['random@gmail.com'];

    describe('VALIDATION_UPDATE_USER', () => {
        before((done) => {
            chai
                .request(app)
                .post(url)
                .send(testUser)
                .end((err, res) => {
                    try {
                        data = responseSuccess(err, res);
                        Object.keys(testUser).forEach(key => {
                            expect(data[key]).to.be.equal(testUser[key]);
                        });
                        testUser.id = data.id;
                        done();
                    } catch (err) {
                        done();
                    }
                });
        });

        it('USER_DOESNT_EXIST', (done) => {
            try {
                randomEmails.forEach((testEmail) => {
                    const user = {...userToInsert, email: testEmail}
                    chai
                        .request(app)
                        .put(`${url}/12345}`)
                        .send(user)
                        .end((err, res) => {
                            try {
                                if(res.status === 404) {
                                    expect(res).to.have.status(404);
                                } else {
                                    expect(res).to.have.status(400);
                                }
                            } catch (err) {
                                done(err);
                            }
                        });
                });
                done();
            } catch(err) {
                done(err);
            }
        });

        it('WRONG_EMAIL', (done) => {
            try {
                wrongEmails.forEach((testEmail) => {
                    const user = {...userToInsert, email: testEmail}
                    chai
                        .request(app)
                        .put(`${url}/${testUser.id}`)
                        .send(user)
                        .end((err, res) => {
                            try {
                                expect(res).to.have.status(400);
                            } catch (err) {
                                done(err);
                            }
                        });
                });
                done();
            } catch(err) {
                done(err);
            }
        });

        it('WRONG_PHONE', (done) => {
            const user = {...testUser, phoneNumber: wrongPhone}
            chai
                .request(app)
                .put(`${url}/${testUser.id}`)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('ID_IS_PRESENT', (done) => {
            const user = {...testUser, id: 111}
            chai
                .request(app)
                .put(`${url}/${testUser.id}`)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('EXTRA_FIELD', (done) => {
            const user = {...testUser, randmoField: 'random'}
            chai
                .request(app)
                .put(`${url}/${testUser.id}`)
                .send(user)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        after((done) => {
            chai
                .request(app)
                .delete(`${url}/${testUser.id}`)
                .send(testUser)
                .end((err, res) => {
                    try {
                        data = responseSuccessDelete(err, res);
                        done();
                    } catch(err) {
                        done(err);
                    }
                });
        });
        
    });
});