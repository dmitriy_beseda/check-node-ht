const { expect } = require('chai');
const chai = require('chai'),
    chaiHttp = require('chai-http');

const { fighterToInsert } = require('./testData');

console.log = () => {};

chai.use(chaiHttp);

const { app } = require('../index');

const responseSuccess = (err, res) => {
    expect(err).to.be.null;
    expect(res).to.have.status(200);
    return JSON.parse(res.text);
};

const responseSuccessDelete = (err, res) => {
    expect(err).to.be.null;
    if(res.status === 204) {
        expect(res).to.have.status(204);
    } else {
        expect(res).to.have.status(200);
        return JSON.parse(res.text);
    }
    return {};
};

const url = '/api/fighters';

describe('LEVEL_4', () => {
    const testFighter = {...fighterToInsert};

    describe('CRUD', () => {
        it('CREATE', (done) => {
            chai
                .request(app)
                .post(url)
                .send(testFighter)
                .end((err, res) => {
                    let data = null;
                    if(err || res.status !== 200) {
                        testFighter.health = 100;

                        chai
                            .request(app)
                            .post(url)
                            .send(testFighter)
                            .end((err, res) => {
                                data = responseSuccess(err, res);
                                try {
                                    Object.keys(testFighter).forEach(key => {
                                        expect(data[key]).to.be.equal(testFighter[key]);
                                    });
                                    testFighter.id = data.id;
                                    done();
                                } catch (err) {
                                    done(err);
                                }
                            });
                    } else {
                        try {
                            data = responseSuccess(err, res);
                            Object.keys(testFighter).forEach(key => {
                                expect(data[key]).to.be.equal(testFighter[key]);
                            });
                            testFighter.id = data.id;
                            done();
                        } catch (err) {
                            done(err);
                        }
                    }
                });
        });

        it('READ', (done) => {
            chai
                .request(app)
                .get(url)
                .end((err, res) => {
                    try {
                        const fighters = responseSuccess(err, res);
                        expect(fighters.length).equal(1);

                        const insertedFighter = fighters[0];

                        Object.keys(testFighter).forEach(key => {
                            expect(insertedFighter[key]).to.be.equal(testFighter[key]);
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('READ_BY_ID', (done) => {
            chai
                .request(app)
                .get(`${url}/${testFighter.id}`)
                .end((err, res) => {
                    try {
                        const response = responseSuccess(err, res);
                        Object.keys(testFighter).forEach(key => {
                            expect(response[key]).to.be.equal(testFighter[key]);
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('UPDATE', (done) => {
            const keyToUpdate = 'name';
            const dataToUpdate = {};
            dataToUpdate[keyToUpdate] = 'Michael';

            chai
                .request(app)
                .put(`${url}/${testFighter.id}`)
                .send(dataToUpdate)
                .end((err, res) => {
                    try {
                        const response = responseSuccess(err, res);
                        Object.keys(testFighter).forEach(key => {
                            if (key !== keyToUpdate) {
                                expect(response[key]).to.be.equal(testFighter[key]);
                            } else {
                                expect(response[key]).equal(dataToUpdate[key]);
                            }
                        });
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('FIGHTER_EXIST_NAME', (done) => {
            const existinFighter = {...testFighter, name: 'klichko'};
            chai
                .request(app)
                .post(url)
                .send(existinFighter)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(400);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });

        it('DELETE', (done) => {
            chai
                .request(app)
                .delete(`${url}/${testFighter.id}`)
                .end((err, res) => {
                    try {
                        responseSuccessDelete(err, res);
                    } catch (err) {
                        done(err);
                    }
                });

            chai
                .request(app)
                .get(`${url}/${testFighter.id}`)
                .end((err, res) => {
                    try {
                        expect(res).to.have.status(404);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
        });
    });
});
